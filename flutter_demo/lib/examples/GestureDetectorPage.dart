import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class GestureDetectorPage extends StatefulWidget {
  const GestureDetectorPage({Key? key}) : super(key: key);

  @override
  _GestureDetectorPageState createState() => _GestureDetectorPageState();
}

class _GestureDetectorPageState extends State<GestureDetectorPage> {
  String _operation = "No Gesture detected!"; //保存事件名

  double _top = 0.0; //距顶部的偏移
  double _left = 0.0; //距左边的偏移

  double _singleTop = 100; //距顶部的偏移

  double _scaleWidth = 200;

  TapGestureRecognizer _tapGestureRecognizer = TapGestureRecognizer();
  bool _toggle = false; //变色开关

  @override
  void dispose() {
    //用到GestureRecognizer的话一定要调用其dispose方法释放资源
    _tapGestureRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('手势检测(单机、双击、长按、拖动)'),
      ),
      body: ConstrainedBox(
        constraints: BoxConstraints.expand(),
        //通过ConstrainedBox来确保Stack占满屏幕 子组件居中才生效
        child: Stack(
          alignment: Alignment.center,
          fit: StackFit.loose,
          children: [
            _tapChild(), //单机、双击、长按手势识别
            _panChild(), //拖动
            _singlePanChild(), //单一方向拖动
            _scaleChild(), //缩放
            _recognizeChild(), //GestureRecognizer
          ],
        ),
      ),
    );
  }



  /*
  * GestureRecognizer
  * */
  Widget _recognizeChild() {
    return Positioned(
      top: 20,
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(text: '你好世界'),
            TextSpan(
              text: '你好世界',
              style: TextStyle(
                fontSize: 30.0,
                color: _toggle ? Colors.blue : Colors.red,
              ),
              recognizer: _tapGestureRecognizer
                ..onTap = () {
                  setState(() {
                    _toggle = !_toggle;
                  });
                },
            ),
            TextSpan(text: '你好世界'),
          ],
        ),
      ),
    );
  }

  /*
  * 缩放 （demo放大没生效）
  * */
  Widget _scaleChild() {
    return Positioned(
      bottom: 10,
      child: GestureDetector(
        child: Image.asset('images/cliphead.png', width: _scaleWidth),
        onScaleUpdate: (ScaleUpdateDetails e) {
          setState(() {
            //缩放倍数在0.8到10倍之间
            _scaleWidth = 200 * e.scale.clamp(.8, 10);
          });
        },
      ),
    );
  }

  /*
  * 单一方向拖动
  * */
  Widget _singlePanChild() {
    return Positioned(
      top: _singleTop,
      child: GestureDetector(
        child: ElevatedButton(
          onPressed: () {},
          child: Text('单一方向拖动'),
        ),
        onVerticalDragUpdate: (DragUpdateDetails e) {
          setState(() {
            _singleTop += e.delta.dy;
          });
        },
      ),
    );
  }

  /*
  * 单机、双击、长按手势识别
  * */
  Widget _tapChild() {
    return GestureDetector(
      child: Container(
        alignment: Alignment.center,
        color: Colors.blue,
        width: 200.0,
        height: 100.0,
        child: Text(
          _operation,
          style: TextStyle(color: Colors.white),
        ),
      ),
      onTap: () => updateText('Tap'),
      onDoubleTap: () => updateText('DoubleTap'),
      onLongPress: () => updateText('LongPress'),
    );
  }

  /*
  * 拖动手势识别
  * */
  Widget _panChild() {
    return Positioned(
      top: _top,
      left: _left,
      child: GestureDetector(
        child: CircleAvatar(
          child: Text('A'),
        ),
        //手指按下
        onPanDown: (DragDownDetails e) {
          print("用户手指按下：${e.globalPosition}");
        },
        //手指滑动
        onPanUpdate: (DragUpdateDetails e) {
          setState(() {
            _left += e.delta.dx;
            _top += e.delta.dy;
          });
        },
        onPanEnd: (DragEndDetails e) {
          //打印滑动结束时在x、y轴上的速度
          print(e.velocity);
        },
      ),
    );
  }

  void updateText(String text) {
    setState(() {
      _operation = text;
    });
  }
}
