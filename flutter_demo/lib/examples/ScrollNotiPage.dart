/*
* 接收到滚动事件时，参数类型为 ScrollNotification，包括一个 metrics 属性，类型是 ScrollMetrics
* 该属性包含当前 ViewPort 及滚动位置等信息
* pixels：当前滚动位置
* maxScrollPosition：最大可滚动长度
* */

import 'package:flutter/material.dart';

class ScrollNotiPage extends StatefulWidget {
  const ScrollNotiPage({Key? key}) : super(key: key);

  @override
  _ScrollNotiPageState createState() => _ScrollNotiPageState();
}

class _ScrollNotiPageState extends State<ScrollNotiPage> {

  String _progress = "0%"; //保存进度百分比

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScrollNotiPage'),
      ),
      body: Scrollbar(
        child: NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification notification) {
            double progress = notification.metrics.pixels / notification.metrics.maxScrollExtent;
            setState(() {
              _progress = "${(progress * 100).toInt()}%";
            });
            return false;
          },
          child: Stack(
            alignment: Alignment.center,
            children: [
              ListView.builder(
                  itemCount: 100,
                  itemExtent: 50.0,
                  itemBuilder: (context, index) => ListTile(title: Text('$index')),
              ),
              CircleAvatar(
                radius: 30.0,
                child: Text(_progress),
                backgroundColor: Colors.black54,
              )
            ],
          ),
        ),
      ),
    );
  }
}
