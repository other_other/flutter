/*
* 使用 Navigator.of(context).pop(..) 方法来关闭对话框，返回一个结果数据
*
* 取消/确认对话框 AlertDialog
* 带一个列表对话框 SimpleDialog
* 带延迟加载组件对话框 Dialog
*
* showDialog 是Material组件库中提供的一个打开Material风格对话框方法
* */

import 'package:flutter/material.dart';

class DialogPage extends StatefulWidget {
  const DialogPage({Key? key}) : super(key: key);

  @override
  _DialogPageState createState() => _DialogPageState();
}

class _DialogPageState extends State<DialogPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dialog'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ElevatedButton(
              onPressed: () async {
                //弹出对话框并等待其关闭
                bool? delete = await showAlertDialog();
                if (delete == null) {
                  print("取消删除");
                } else {
                  print("已确认删除");
                }
              },
              child: Text('取消/删除对话框'),
            ),
            ElevatedButton(
              onPressed: showSimpleDialog,
              child: Text('一个列表的对话框'),
            ),
            ElevatedButton(
              onPressed: showListDialog,
              child: Text('嵌套ListView的对话框'),
            ),
            ElevatedButton(
              onPressed: showBottomSheet,
              child: Text('底部菜单列表对话框'),
            ),
          ],
        ),
      ),
    );
  }

  /*
  * 弹出底部列表对话框
  * */
  Future<int?> showBottomSheet() async {
    int? index = await showModalBottomSheet(
      context: context,
      builder: (context) {
        return ListView.builder(
          itemCount: 20,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text('$index'),
              onTap: () => Navigator.of(context).pop(index),
            );
          },
        );
      },
    );
    if (index != null) {

    }
  }

  /*
  * 嵌套ListView的对话框
  * */
  Future<void> showListDialog() async {
    int? index = await showDialog(
      context: context,
      builder: (context) {
        var child = Column(
          children: [
            ListTile(title: Text('请选择')),
            Expanded(
              child: ListView.builder(
                itemCount: 20,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text('$index'),
                    onTap: () => Navigator.of(context).pop(index),
                  );
                },
              ),
            ),
          ],
        );
        return Dialog(
          child: child,
        );
      },
    );
    if (index != null) {
      print("点击了：$index");
    }
  }

  /*
  * 显示一个列表的对话框
  * */
  Future<void> showSimpleDialog() async {
    int? i = await showDialog<int>(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text('请选择语言'),
          children: [
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, 1); //返回1
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 6),
                child: Text('中文简体'),
              ),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, 2); //返回2
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 6),
                child: Text('美国英语'),
              ),
            ),
          ],
        );
      },
    );
    if (i != null) {
      print("选择了：${i == 1 ? "中文简体" : "美国英语"}");
    }
  }

  /*
  * 取消/删除对话框
  * */
  Future<bool?> showAlertDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('提示'),
          content: Text('您确定要删除当前文件吗?'),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('取消'),
            ),
            TextButton(
              onPressed: () {
                //关闭对话框并返回true
                Navigator.of(context).pop(true);
              },
              child: Text('删除'),
            ),
          ],
        );
      },
    );
  }
}
