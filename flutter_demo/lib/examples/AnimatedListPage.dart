import 'package:flutter/material.dart';

class AnimatedListPage extends StatefulWidget {
  const AnimatedListPage({Key? key}) : super(key: key);

  @override
  _AnimatedListPageState createState() => _AnimatedListPageState();
}

class _AnimatedListPageState extends State<AnimatedListPage> {
  var data = <String>[];
  int counter = 5;
  final globalKey = GlobalKey<AnimatedListState>();

  @override
  void initState() {
    super.initState();
    for (var i = 0; i < counter; i++) {
      data.add('${i + 1}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedListPage'),
      ),
      body: Stack(
        children: [
          AnimatedList(
            key: globalKey,
            initialItemCount: data.length,
            itemBuilder:
                (BuildContext context, int index, Animation<double> animation) {
              //添加列表项时会执行渐显动画
              return FadeTransition(
                opacity: animation,
                child: _buildItem(context, index),
              );
            },
          ),
          _buildAddBtn(),
        ],
      ),
    );
  }

  Widget _buildItem(context, index) {
    String char = data[index];
    return ListTile(
      key: ValueKey(char),
      title: Text(char),
      trailing: IconButton(
          onPressed: () {
            _onDelete(context, index);
          },
          icon: Icon(Icons.delete)),
    );
  }

  void _onDelete(context, index) {
    setState(() {
      globalKey.currentState!.removeItem(index, (context, animation) {
        var item = _buildItem(context, index);
        data.removeAt(index);
        // 删除动画是一个合成动画：渐隐 + 缩小列表项告诉
        return FadeTransition(
          opacity: CurvedAnimation(
            parent: animation,
            //让透明度变化的更快一些
            curve: const Interval(0.5, 1.0),
          ),
          // 不断缩小列表项的高度
          child: SizeTransition(
            sizeFactor: animation,
            axisAlignment: 0.0,
            child: item,
          ),
        );
      });
    });
  }

  void _onAdd() {
    // 添加一个列表项
    data.add('${++counter}');
    // 告诉列表项有新添加的列表项
    globalKey.currentState!.insertItem(data.length - 1);
    print('添加 $counter');
  }

  // 创建一个 “+” 按钮
  Widget _buildAddBtn() {
    return Positioned(
        left: 0,
        right: 0,
        bottom: 30,
        child: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: _onAdd,
        ));
  }
}
