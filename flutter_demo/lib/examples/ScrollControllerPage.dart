/*
* ListView 滚动位置发生变化时打印滚动位置，超过1000像素屏幕右下角显示返回顶部按钮
* 点击按钮ListView恢复到初始位置
* */

import 'package:flutter/material.dart';

class ScrollControllerPage extends StatefulWidget {
  const ScrollControllerPage({Key? key}) : super(key: key);

  @override
  _ScrollControllerPageState createState() => _ScrollControllerPageState();
}

class _ScrollControllerPageState extends State<ScrollControllerPage> {
  ScrollController _controller = ScrollController();
  bool showToTopBtn = false; //是否显示“返回到顶部”按钮

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      print(_controller.offset); //打印滚动位置
      if (_controller.offset < 1000 && showToTopBtn) {
        setState(() {
          showToTopBtn = false;
        });
      } else if (_controller.offset >= 1000 && showToTopBtn == false) {
        setState(() {
          showToTopBtn = true;
        });
      }
    });
  }

  @override
  void dispose() {
    //为了避免内存泄露，需要调用_controller.dispose
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScrollControllerPage'),
      ),
      body: ListView.builder(
        itemCount: 100,
        itemExtent: 50.0, //列表项高度固定时 性能消耗较小
        controller: _controller,
        itemBuilder: (context, index) {
          return ListTile(title: Text('$index'));
        },
      ),
      floatingActionButton: !showToTopBtn
          ? null
          : FloatingActionButton(
              child: Icon(Icons.arrow_upward),
              onPressed: () {
                _controller.animateTo(.0,
                    duration: Duration(milliseconds: 200), curve: Curves.ease);
              },
            ),
    );
  }
}
