/*
* allowImplicitScrolling 为true 则 PageView 会前后各缓存一个页面宽度
*
* 可滚动组件子项缓存 KeepAlive
* 只需让 PageState 混入 AutomaticKeepAliveClientMixin
* wantKeepAlive返回true 调用super.build
* */

import 'package:flutter/material.dart';

class PageViewPage extends StatefulWidget {
  const PageViewPage({Key? key}) : super(key: key);

  @override
  _PageViewPageState createState() => _PageViewPageState();
}

class _PageViewPageState extends State<PageViewPage> {
  @override
  Widget build(BuildContext context) {

    var children = <Widget>[];
    for (int i = 0; i < 6; i++) {
      children.add(Page(text: '$i'));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('PageViewPage'),
      ),
      body: PageView(
        // allowImplicitScrolling: true, //true 前后各缓存一个页面宽度
        children: children,
      ),
    );
  }
}

class Page extends StatefulWidget {
  const Page({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<Page> with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true; // 是否需要缓存

  @override
  Widget build(BuildContext context) {
    super.build(context);// 必须调用
    print("build ${widget.text}");
    return Center(
      child: Text('${widget.text}', textScaleFactor: 5),
    );
  }
}
