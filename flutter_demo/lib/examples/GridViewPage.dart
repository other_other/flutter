/*
* GridView 构建一个二维网格列表
* gridDelegate 控制GridView子组件如何排列
* SliverGridDelegateWithFixedCrossAxisCount 该子类实现了一个横轴为固定数量子元素的 layout 算法
* SliverGridDelegateWithMaxCrossAxisExtent 该子类实现了一个横轴为最大长度的 layout 算法
*
* GridView.count 快速的创建横轴固定数量子元素的GridView
* GridView.extert 快速的创建横轴子元素为固定最大长度的的GridView
* */

import 'package:flutter/material.dart';

class GridViewPage extends StatefulWidget {
  const GridViewPage({Key? key}) : super(key: key);

  @override
  _GridViewPageState createState() => _GridViewPageState();
}

class _GridViewPageState extends State<GridViewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GridViewPage'),
      ),
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(flex: 1, child: _buildGridViewLayout1()),
          Expanded(flex: 1, child: _buildGridViewLayout2()),
        ],
      ),
    );
  }

  /*
  * SliverGridDelegateWithFixedCrossAxisCount
  * 横轴固定数量layout
  * */
  Widget _buildGridViewLayout1() {
    return GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3, //横轴三个子widget
        childAspectRatio: 1.0, //宽高比为1时，子widget
      ),
      children: [
        Icon(Icons.ac_unit),
        Icon(Icons.airport_shuttle),
        Icon(Icons.all_inclusive),
        Icon(Icons.beach_access),
        Icon(Icons.cake),
        Icon(Icons.free_breakfast),
      ],
    );
  }

  /*
  * 横轴固定数量layout
  * */
  Widget _buildGridViewLayout3() {
    return GridView.count(
      crossAxisCount: 3,
      childAspectRatio: 1.0,
      children: [
        Icon(Icons.ac_unit),
        Icon(Icons.airport_shuttle),
        Icon(Icons.all_inclusive),
        Icon(Icons.beach_access),
        Icon(Icons.cake),
        Icon(Icons.free_breakfast),
      ],
    );
  }

  /*
  * SliverGridDelegateWithMaxCrossAxisExtent
  * 横轴子元素为固定最大长度的layout算法
  * */
  Widget _buildGridViewLayout2() {
    return GridView(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 120,
        childAspectRatio: 2.0, //宽高比为2
      ),
      children: [
        Icon(Icons.ac_unit),
        Icon(Icons.airport_shuttle),
        Icon(Icons.all_inclusive),
        Icon(Icons.beach_access),
        Icon(Icons.cake),
        Icon(Icons.free_breakfast)
      ],
    );
  }

  /*
  * 横轴子元素为固定最大长度的layout算法
  * */
  Widget _buildGridViewLayout4() {
    return GridView.extent(
      maxCrossAxisExtent: 120,
      childAspectRatio: 2.0,
      children: [
        Icon(Icons.ac_unit),
        Icon(Icons.airport_shuttle),
        Icon(Icons.all_inclusive),
        Icon(Icons.beach_access),
        Icon(Icons.cake),
        Icon(Icons.free_breakfast)
      ],
    );
  }

}
