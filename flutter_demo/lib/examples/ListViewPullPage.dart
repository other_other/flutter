/*
* 上拉加载列表，滑动到底部，表尾显示一个Loading，拉取数据插入到表尾
* */

import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';

class ListViewPullPage extends StatefulWidget {
  const ListViewPullPage({Key? key}) : super(key: key);

  @override
  _ListViewPullPageState createState() => _ListViewPullPageState();
}

class _ListViewPullPageState extends State<ListViewPullPage> {
  static const loadingTag = "##loading##"; //表尾标记
  var _words = <String>[loadingTag];

  @override
  void initState() {
    super.initState();
    _restrieveData();
  }

  void _restrieveData() {
    Future.delayed(Duration(seconds: 2)).then((value) {
      setState(() {
        //重新构建列表
        _words.insertAll(
          _words.length - 1,
          //每次生成20个单词
          generateWordPairs().take(20).map((e) => e.asPascalCase).toList(),
        );
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListViewPullPage'),
      ),
      body: Column(
        children: [
          ListTile(title: Text('表头')),
          Divider(color: Colors.red, height: 1.0),
          Expanded(
            child: ListView.separated(
              itemBuilder: (BuildContext context, int index) {
                //如果到了表尾
                if (_words[index] == loadingTag) {
                  //不足100条，继续获取数据
                  if (_words.length - 1 < 100) {
                    _restrieveData();
                    //加载时显示loading
                    return Container(
                      padding: EdgeInsets.all(16.0),
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: 24.0,
                        height: 24.0,
                        child: CircularProgressIndicator(strokeWidth: .0),
                      ),
                    );
                  } else {
                    //已经加载了100条数据，不再获取数据。
                    return Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(16.0),
                      child: Text('没有更多了', style: TextStyle(color: Colors.grey)),
                    );
                  }
                } else {
                  return ListTile(title: Text(_words[index]));
                }
              },
              separatorBuilder: (context, index) =>
                  Divider(color: Colors.red, height: 1.0),
              itemCount: _words.length,
            ),
          ),
        ],
      ),
    );
  }
}
