import 'package:flutter/material.dart';

class NestedScrollViewPage extends StatefulWidget {
  const NestedScrollViewPage({Key? key}) : super(key: key);

  @override
  _NestedScrollViewPageState createState() => _NestedScrollViewPageState();
}

class _NestedScrollViewPageState extends State<NestedScrollViewPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverAppBar(
              title: Text('NestedScrollViewPage'),
              pinned: true, // 固定在顶部
              forceElevated: innerBoxIsScrolled,
            ),
            _buildSliverList(5),
          ];
        },
        body: ListView.builder(
          padding: EdgeInsets.all(8.0),
          physics: ClampingScrollPhysics(),
          itemCount: 30,
          itemBuilder: (BuildContext context, int index) {
            return SizedBox(
              height: 50,
              child: Center(child: Text('Item $index')),
            );
          },
        ),
      ),
    );
  }

  Widget _buildSliverList([int count = 5]) {
    return SliverFixedExtentList(
      delegate: SliverChildBuilderDelegate((context, index) {
        return ListTile(title: Text('$index'));
      }, childCount: count),
      itemExtent: 50,
    );
  }
}
