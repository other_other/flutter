
/*
* Stack 和 Positioned，我们可以指定一个或多个子元素相对父元素各个边的偏移，并且可以堆叠；
* 如果只想简单调整下一个子元素在父元素中的位置的话使用 Align 组件更简单
* widthFactor，heightFactor 用于确定 Align 组件本身宽高属性，是两个缩放因子，如果为 null，则组件的宽高会占用尽可能多的空间
* Alignment(this.x, this.y)
* 会以矩形中心点作为原点，x y 值从 -1 到 1 分别代表矩形左边到右边的距离和顶部到底部的距离，Alignment(-1,-1) 代表左上角顶点
* FractionalOffset 继承自 Alignment，唯一区别就是坐标原点不同，坐标原点为矩形左侧顶点
* 实际偏移 = (FractionalOffse.x * childWidth, FractionalOffse.y * childHeight)
* (0.2*60, 0.6*60)
* */

import 'package:flutter/material.dart';


class AlignPage extends StatefulWidget {
  const AlignPage({Key? key}) : super(key: key);

  @override
  _AlignPageState createState() => _AlignPageState();
}

class _AlignPageState extends State<AlignPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AlignPage'),
      ),
      body: Column(
        children: [
          Container(
            height: 120.0,
            width: 120.0,
            color: Colors.blue.shade50,
            child: Align(
              alignment: Alignment.topRight,
              child: FlutterLogo(
                size: 60,
              ),
            ),
          ),
          Container(
            color: Colors.blue,
            child: Align(
              widthFactor: 2,//确定Align组件宽度 2*子组件宽60=120
              heightFactor: 2,
              alignment: Alignment.topRight,
              child: FlutterLogo(
                size: 60,
              ),
            ),
          ),
          Container(
            color: Colors.greenAccent,
            child: Align(
              widthFactor: 2,
              heightFactor: 2,
              alignment: Alignment(2, 0.0),
              child: FlutterLogo(
                size: 60,
              ),
            ),
          ),
          Container(
            color: Colors.yellow,
            child: Align(
              widthFactor: 2,
              heightFactor: 2,
              alignment: FractionalOffset(0.2, 0.6),
              child: FlutterLogo(
                size: 60,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
