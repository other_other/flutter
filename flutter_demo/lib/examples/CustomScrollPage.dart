
/*
* CustomScrollView 主要功能是提供一个公共的Scrollable和ViewPort来组合多个Sliver
* */

import 'package:flutter/material.dart';

class CustomScrollPage extends StatefulWidget {
  const CustomScrollPage({Key? key}) : super(key: key);

  @override
  _CustomScrollPageState createState() => _CustomScrollPageState();
}

class _CustomScrollPageState extends State<CustomScrollPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CustomScrollView'),
      ),
      body: _buildTwoSliverList(),
    );
  }

  Widget _buildTwoSliverList() {
    var listView = SliverFixedExtentList(
      delegate: SliverChildBuilderDelegate(
          (context, index) => ListTile(title: Text('$index'))),
      itemExtent: 56, //列表项高度固定
    );
    return CustomScrollView(
      slivers: [
        listView,
        listView,
      ],
    );
  }
}
