
/*
* Stack Postioned
* 第一个文本组件没有指定定位会根据algment居中显示
* aligment：没有指定定位或部分指定定位的子组件对齐方式
* fit：没有指定定位的子组件如何适应Stack的大小，默认StackFit.loose使用子组件大小，
* StackFit.expand表示扩展到Stack的大小
* */

import 'package:flutter/material.dart';

class StackPage extends StatefulWidget {
  const StackPage({Key? key}) : super(key: key);

  @override
  _StackPageState createState() => _StackPageState();
}

class _StackPageState extends State<StackPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stack'),
      ),
      body: ConstrainedBox(
        //通过ConstrainedBox来确保Stack占满屏幕
        constraints: BoxConstraints.expand(),
        child: Stack(
          alignment: Alignment.center,
          fit: StackFit.loose,
          children: [
            Container(
              child: Text('Hello world', style: TextStyle(color: Colors.white)),
              color: Colors.red,
            ),
            Positioned(
              left: 18.0,
              child: Text('I am Jack'),
            ),
            Positioned(
              top: 18.0,
              child: Text('Your friend'),
            ),
          ],
        ),
      ),
    );
  }
}
