
import 'package:flutter/material.dart';

class ClipPage extends StatelessWidget {
  const ClipPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget avatar = Image(image: AssetImage('images/cliphead.png'), width: 100.0,);
    return Scaffold(
      appBar: AppBar(
        title: Text('ClipPage'),
      ),
      body: Center(
        child: Column(
          children: [
            avatar, //不裁剪
            ClipOval(child: avatar),//裁剪为圆形
            ClipRRect(//裁剪为圆角矩形
              borderRadius: BorderRadius.circular(5.0),
              child: avatar,
            ),
            ClipRect(
              child: Align(
                alignment: Alignment.topLeft,
                widthFactor: .5,//宽度设为原来宽度一半 裁剪溢出部分
                child: avatar,
              ),
            ),
            DecoratedBox(
              decoration: BoxDecoration(color: Colors.red),
              child: ClipRect(
                clipper: MyClipper(),//裁剪 40x30
                child: avatar,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Rect> {

  @override
  //获取裁剪区域
  Rect getClip(Size size) => Rect.fromLTWH(10.0, 15.0, 40.0, 30.0);

  @override
  //决定是否重新剪裁。如果在应用中，剪裁区域始终不会发生变化时应该返回false，这样就不会触发重新剪裁，
  //避免不必要的性能开销。如果剪裁区域会发生变化（比如在对剪裁区域执行一个动画），那么变化后应该返回true来重新执行剪裁
  bool shouldReclip(covariant CustomClipper<Rect> oldClipper) => false;

}





