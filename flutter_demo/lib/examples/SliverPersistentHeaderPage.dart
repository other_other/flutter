import 'package:flukit/flukit.dart';
import 'package:flutter/material.dart';

class SliverPersistentHeaderPage extends StatefulWidget {
  const SliverPersistentHeaderPage({Key? key}) : super(key: key);

  @override
  _SliverPersistentHeaderPageState createState() =>
      _SliverPersistentHeaderPageState();
}

class _SliverPersistentHeaderPageState
    extends State<SliverPersistentHeaderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SliverPersistentHeader'),
      ),
      body: CustomScrollView(
        slivers: [
          _buildSliverList(),
          SliverPersistentHeader(
            pinned: true,
            delegate: SliverHeaderDelegate(
              //有最大和最小高度
              maxHeight: 80,
              minHeight: 50,
              child: _buildHeader(1),
            ),
          ),
          _buildSliverList(20),
          SliverPersistentHeader(
            pinned: true,
            delegate: SliverHeaderDelegate.fixedHeight(
              height: 50,
              child: _buildHeader(2),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSliverList([int count = 5]) {
    return SliverFixedExtentList(
      delegate: SliverChildBuilderDelegate((context, index) {
        return ListTile(title: Text('$index'));
      }, childCount: count),
      itemExtent: 50,
    );
  }

  Widget _buildHeader(int i) {
    return Container(
      color: Colors.lightBlue.shade200,
      alignment: Alignment.centerLeft,
      child: Text("PersistentHeader $i"),
    );
  }
}
