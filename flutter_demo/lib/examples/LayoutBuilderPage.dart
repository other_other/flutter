/*
* 通过 LayoutBuilder，我们可以在布局过程中拿到父组件传递的约束信息，然后我们可以根据约束信息动态的构建不同的布局
* */

import 'package:flutter/material.dart';

class LayoutBuilderPage extends StatefulWidget {
  const LayoutBuilderPage({Key? key, required this.children}) : super(key: key);

  final List<Widget> children;

  @override
  _LayoutBuilderPageState createState() => _LayoutBuilderPageState();
}

class _LayoutBuilderPageState extends State<LayoutBuilderPage> {

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      // 最大宽度小于200，显示单列
      if (constraints.maxWidth < 200) {
        return Column(children: widget.children, mainAxisSize: MainAxisSize.min);
      } else {
        // 大于200，显示双列
        var _children = <Widget>[];
        for (var i = 0; i < widget.children.length; i += 2) {
          if (i + 1 < widget.children.length) {
            _children.add(Row(
              children: [widget.children[i], widget.children[i + 1]],
              mainAxisSize: MainAxisSize.min,
            ));
          } else {
            _children.add(widget.children[i]);
          }
        }
        return Column(children: _children, mainAxisSize: MainAxisSize.min);
      }
    });
  }
}

class LayoutBuilderRoute extends StatelessWidget {

  var _children = List.filled(6, Text('A'));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('LayoutBuilderPage'),
      ),
      body: Column(
        children: [
          SizedBox(
            width: 180, // 限制宽度为190，小于 200
            child: LayoutBuilderPage(children: _children),
          ),
          LayoutBuilderPage(children: _children),
        ],
      ),
    );
  }
}
