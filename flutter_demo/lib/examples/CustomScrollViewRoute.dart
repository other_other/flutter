/*
* SliverToBoxAdapter 适配器，可以将RenderBoxs适配为Sliver
* */

import 'package:flutter/material.dart';

class CustomScrollViewRoute extends StatefulWidget {
  const CustomScrollViewRoute({Key? key}) : super(key: key);

  @override
  _CustomScrollViewRouteState createState() => _CustomScrollViewRouteState();
}

class _CustomScrollViewRouteState extends State<CustomScrollViewRoute> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: CustomScrollView(
        slivers: [
          // AppBar，包含一个导航栏
          SliverAppBar(
            pinned: true, // 滑动到顶端时会固定住
            expandedHeight: 250,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('CustomScrollViewRoute'),
              background: Image.asset('images/cliphead.png', fit: BoxFit.cover),
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.all(8.0),
            sliver: SliverGrid(
              delegate: SliverChildBuilderDelegate((context, index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.cyan[100 * (index % 9)],
                  child: Text('grid item $index'),
                );
              }, childCount: 20),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: 4,
              ),
            ),
          ),
          SliverFixedExtentList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * (index % 9)],
                  child: Text('list item $index'),
                );
              }, childCount: 20),
              itemExtent: 50.0),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 300,
              child: PageView(
                children: [
                  Text('1', textScaleFactor: 5),
                  Text('2', textScaleFactor: 5),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
