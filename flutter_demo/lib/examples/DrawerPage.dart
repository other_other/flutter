


import 'package:flutter/material.dart';


class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DrawerPage'),
      ),
      drawer: MyDrawer(),
    );
  }
}

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: MediaQuery.removePadding(
          removeTop: true, //移除抽屉菜单顶部默认留白 Drawer默认顶部会留和手机状态栏等高的留白
          context: context,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 38.0),
                child: Row(
                  children: [
                    Padding(
                      //加水平间距
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: ClipOval(
                        child: Image.asset(
                          'images/cliphead.png',
                          width: 80.0,
                        ),
                      ),
                    ),
                    Text('Wendux',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  children: [
                    ListTile(
                      leading: Icon(Icons.add),
                      title: Text('Add account'),
                    ),
                    ListTile(
                      leading: Icon(Icons.settings),
                      title: Text('Manage accounts'),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}