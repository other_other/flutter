
/*
* Row 和 Colum 时，如果子 widget 超出屏幕范围，则会报溢出错误
* 超出屏幕显示范围会自动折行的布局称为流式布局
* Flutter中通过Wrap和Flow来支持流式布局
* */

import 'package:flutter/material.dart';


class WrapPage extends StatefulWidget {
  const WrapPage({Key? key}) : super(key: key);

  @override
  _WrapPageState createState() => _WrapPageState();
}

class _WrapPageState extends State<WrapPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FlexPage'),
      ),
      body: Wrap(
        spacing: 8,//水平方向间距
        runSpacing: 8,//垂直方向间距
        alignment: WrapAlignment.center,
        children: [
          Chip(
              avatar: CircleAvatar(backgroundColor: Colors.blue, child: Text('A')),
              label: Text('Hamilton'),
          ),
          Chip(
            avatar: CircleAvatar(backgroundColor: Colors.blue, child: Text('N')),
            label: Text('Lafayette'),
          ),
          Chip(
            avatar: CircleAvatar(backgroundColor: Colors.blue, child: Text('B')),
            label: Text('Mulligan'),
          ),
          Chip(
            avatar: CircleAvatar(backgroundColor: Colors.blue, child: Text('J')),
            label: Text('Laurens'),
          ),
        ],
      ),
    );
  }
}
