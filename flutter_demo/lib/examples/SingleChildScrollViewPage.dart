/*
* SingleChildScrollView
* 只应在期望的内容不会超过屏幕太多时使用，预计视口可能包含超出屏幕尺寸太多的内容时
* 应该使用一些支持 Sliver 延迟加载的可滚动组件，如 ListView
* */

import 'package:flutter/material.dart';

class SingleChildScrollViewPage extends StatefulWidget {
  const SingleChildScrollViewPage({Key? key}) : super(key: key);

  @override
  _SingleChildScrollViewPageState createState() =>
      _SingleChildScrollViewPageState();
}

class _SingleChildScrollViewPageState extends State<SingleChildScrollViewPage> {
  @override
  Widget build(BuildContext context) {
    String str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return Scaffold(
      appBar: AppBar(
        title: Text('SingleChildScrollView'),
      ),
      body: Scrollbar(//显示进度条
        child: SingleChildScrollView(
          padding: EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              children: str.split("") //每一个字母都用一个Text显示,字体为原来的两倍
                  .map((e) => Text(e, textScaleFactor: 2.0))
                  .toList(),
            ),
          ),
        ),
      ),
    );
  }
}
