import 'package:flutter/material.dart';

class ValuelistenableBuilderPage extends StatefulWidget {
  const ValuelistenableBuilderPage({Key? key}) : super(key: key);

  @override
  _ValuelistenableBuilderPageState createState() =>
      _ValuelistenableBuilderPageState();
}

class _ValuelistenableBuilderPageState
    extends State<ValuelistenableBuilderPage> {
  final ValueNotifier<int> _counter = ValueNotifier<int>(0);
  static const double textScaleFactor = 1.5;

  @override
  Widget build(BuildContext context) {
    // 添加 + 按钮不会触发整个 ValueListenableRoute 组件的 build
    print('build');

    return Scaffold(
      appBar: AppBar(
        title: Text('ValuelistenableBuilder'),
      ),
      body: Center(
        child: ValueListenableBuilder(
          valueListenable: _counter, //监听 _counter值
          builder: (BuildContext context, int value, Widget? child) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                child!,
                Text('$value 次', textScaleFactor: textScaleFactor),
              ],
            );
          },
          child: const Text('点击了 ', textScaleFactor: textScaleFactor),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _counter.value += 1, //_counter 值改变
        child: Icon(Icons.add),
      ),
    );
  }
}
