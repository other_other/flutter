import 'package:flukit/flukit.dart';
import 'package:flutter/material.dart';

class TabBarViewPage2 extends StatefulWidget {
  const TabBarViewPage2({Key? key}) : super(key: key);

  @override
  _TabBarViewPage2State createState() => _TabBarViewPage2State();
}

class _TabBarViewPage2State extends State<TabBarViewPage2> {
  List tabs = ["新闻", "历史", "图片"];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: tabs.length,
        child: Scaffold(
          appBar: AppBar(
            title: Text('TabBarViewPage'),
            bottom: TabBar(
              tabs: tabs.map((e) => Tab(text: e)).toList(),
            ),
          ),
          body: TabBarView(
            children: tabs.map((e) {
              return KeepAliveWrapper(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(e, textScaleFactor: 5),
                ),
              );
            }).toList(),
          ),
        ),
    );
  }
}
