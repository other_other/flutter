
/*
* 空间适配FittedBox
* FittedBox对子组件布局结束后就可以获得子组件真实的大小
* 第一个 父组件比子组件大父组件红色看不到了
* 第二个 按子组件比例缩放尽可能占据父组件空间 按比例适配父组件后父组件就能显示部分了
* */

import 'package:flutter/material.dart';

class FittedBoxPage extends StatelessWidget {
  const FittedBoxPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FittedBoxPage'),
      ),
      body: Center(
        child: Column(
          children: [
            wContainer(BoxFit.none),
            Text('Wendux'),
            wContainer(BoxFit.contain),
            Text('Flutter中国'),
          ],
        ),
      ),
    );
  }

  Widget wContainer(BoxFit boxFit) {
    return Container(
      width: 50,
      height: 50,
      color: Colors.red,
      child: FittedBox(
        fit: boxFit,
        child: Container(
          width: 60,
          height: 70,
          color: Colors.blue,
        ),
      ),
    );
  }
}
