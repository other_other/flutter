/*
* ListView
* ListView.builder itemCount:列表项数量，为null则为无限列表
* itemExtent 和 prototypeItem 互斥
*
* ListView.separated 多了一个 separatorBuilder 参数，列表项之前添加分割线组件
* */

import 'package:flutter/material.dart';

class ListViewPage extends StatefulWidget {
  const ListViewPage({Key? key}) : super(key: key);

  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
  @override

  /*
  * ListView.builder
  * */
  Widget _builderView() {
    return ListView.builder(
      itemCount: 50,
      prototypeItem: ListTile(title: Text("1")), //指定原型
      // itemExtent: 50,//指定列表项高度
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title: Text('$index'),
        );
      },
    );
  }

  /*
  * ListView.separated
  * */
  Widget _separatedView() {
    Widget divider1 = Divider(color: Colors.blue);
    Widget divider2 = Divider(color: Colors.red);
    return ListView.separated(
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text('$index'),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return index % 2 == 0 ? divider1 : divider2;
        },
        itemCount: 50
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListViewPage'),
      ),
      body: Flex(direction: Axis.horizontal, children: [
        Expanded(
          flex: 1,
          child: _builderView(),
        ),
        Expanded(
          flex: 1,
          child: _separatedView(),
        ),
      ]),
    );
  }
}
