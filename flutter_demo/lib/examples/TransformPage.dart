/*
* Transform的变换是应用在绘制阶段，而并不是应用在布局(layout)阶段
* 所以无论对子组件应用何种变化，其占用空间的大小和在屏幕上的位置都是固定不变的
* RotatedBox
* RotatedBox的变换是在layout阶段，会影响在子组件的位置和大小，比如放大后组件的大小也放大了
* */

import 'package:flutter/material.dart';
import 'dart:math' as math;

class TransformPage extends StatelessWidget {
  const TransformPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TransformPage'),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            DecoratedBox(
              decoration: BoxDecoration(
                color: Colors.red,
              ),
              child: Transform.translate( //平移
                offset: Offset(-20.0, -5.0),
                child: Text("Hello world"),
              ),
            ),
            Container(
              color: Colors.greenAccent,
              child: Transform.rotate(
                angle: math.pi/2,
                child: Text("Hello world"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
