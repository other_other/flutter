import 'package:flutter/cupertino.dart';

class HomeData {
  final String title;
  final String subTitle;
  final Widget routerPage;

  HomeData({
    required this.title,
    required this.subTitle,
    required this.routerPage,
  });
}
