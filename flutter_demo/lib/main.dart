

import 'package:flutter/material.dart';
import 'package:flutter_demo/home_page.dart';

void main() {
  return runApp(DemoApp());
}


class DemoApp extends StatelessWidget {
  const DemoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      home: HomePage(),
    );
  }
}




