import 'package:flutter/material.dart';
import 'package:flutter_demo/examples/AlignPage.dart';
import 'package:flutter_demo/examples/AnimatedListPage.dart';
import 'package:flutter_demo/examples/BottomAppBarPage.dart';
import 'package:flutter_demo/examples/ClipPage.dart';
import 'package:flutter_demo/examples/ContainerPage.dart';
import 'package:flutter_demo/examples/CustomScrollPage.dart';
import 'package:flutter_demo/examples/CustomScrollViewRoute.dart';
import 'package:flutter_demo/examples/DecoratedBoxPage.dart';
import 'package:flutter_demo/examples/DialogPage.dart';
import 'package:flutter_demo/examples/DrawerPage.dart';
import 'package:flutter_demo/examples/FittedBoxPage.dart';
import 'package:flutter_demo/examples/FlexPage.dart';
import 'package:flutter_demo/examples/FutureBuilderPage.dart';
import 'package:flutter_demo/examples/GestureDetectorPage.dart';
import 'package:flutter_demo/examples/GridViewBuilderPage.dart';
import 'package:flutter_demo/examples/GridViewPage.dart';
import 'package:flutter_demo/examples/HttpClinePage.dart';
import 'package:flutter_demo/examples/InheritedWidgetPage.dart';
import 'package:flutter_demo/examples/LayoutBuilderPage.dart';
import 'package:flutter_demo/examples/ListViewPage.dart';
import 'package:flutter_demo/examples/ListViewPullPage.dart';
import 'package:flutter_demo/examples/ListenerPage.dart';
import 'package:flutter_demo/examples/NestedScrollViewPage.dart';
import 'package:flutter_demo/examples/NestedTabBarViewPage.dart';
import 'package:flutter_demo/examples/PaddingPage.dart';
import 'package:flutter_demo/examples/PageViewPage.dart';
import 'package:flutter_demo/examples/ScaffoldPage.dart';
import 'package:flutter_demo/examples/ScrollControllerPage.dart';
import 'package:flutter_demo/examples/ScrollNotiPage.dart';
import 'package:flutter_demo/examples/SingleChildScrollViewPage.dart';
import 'package:flutter_demo/examples/SliverAppBarPage.dart';
import 'package:flutter_demo/examples/SliverPersistentHeaderPage.dart';
import 'package:flutter_demo/examples/StackPage.dart';
import 'package:flutter_demo/examples/StreamBuilderPage.dart';
import 'package:flutter_demo/examples/TabBarViewPage.dart';
import 'package:flutter_demo/examples/TabBarViewPage2.dart';
import 'package:flutter_demo/examples/ThemePage.dart';
import 'package:flutter_demo/examples/TransformPage.dart';
import 'package:flutter_demo/examples/ValueLisenableBuilderPage.dart';
import 'package:flutter_demo/examples/WillPopScopePage.dart';
import 'package:flutter_demo/examples/WrapPage.dart';
import 'package:flutter_demo/models/home_data.dart';

class HomePage extends StatelessWidget {
  final List<HomeData> _datas = [
    HomeData(
        title: 'httpCline',
        subTitle: 'httpCline发起请求',
        routerPage: HttpTestRoute()),
    HomeData(
        title: '弹性布局Flex',
        subTitle: '子组件按照一定比例来分配父容器空间',
        routerPage: FlexPage()),
    HomeData(
        title: '流式布局WrapPage',
        subTitle: '超出屏幕显示范围会自动折行的布局',
        routerPage: WrapPage()),
    HomeData(
        title: '层叠布局Stack、Positioned',
        subTitle: 'Stack、Positioned',
        routerPage: StackPage()),
    HomeData(title: '对齐与相对定位Align', subTitle: 'Align', routerPage: AlignPage()),
    HomeData(
        title: 'LayoutBuilder',
        subTitle: '布局过程中拿到父组件传递的约束信息，然后可以根据约束信息动态的构建不同的布局',
        routerPage: LayoutBuilderRoute()),
    HomeData(
        title: '填充PaddingPage',
        subTitle: '给子节点添加填充（留白）',
        routerPage: PaddingPage()),
    HomeData(
        title: '装饰容器DecoratedBox',
        subTitle: '添加背景、边框、渐变',
        routerPage: DecoratedBoxPage()),
    HomeData(title: '变换Transform', subTitle: '', routerPage: TransformPage()),
    HomeData(title: 'Container', subTitle: '', routerPage: ContainerPage()),
    HomeData(title: '裁剪Clip', subTitle: '', routerPage: ClipPage()),
    HomeData(title: '空间适配FittedBox', subTitle: '', routerPage: FittedBoxPage()),
    HomeData(title: 'ScaffoldPage', subTitle: '', routerPage: ScaffoldPage()),
    HomeData(title: '抽屉Drawer', subTitle: '', routerPage: DrawerPage()),
    HomeData(title: 'BottomAppBar', subTitle: '底部Tab导航栏', routerPage: BottomAppBarPage()),
    HomeData(title: 'SingleChildScrollView', subTitle: '内容不会超过屏幕太多时使用', routerPage: SingleChildScrollViewPage()),
    HomeData(title: 'ListView', subTitle: 'ListView.builder/ListView.separated', routerPage: ListViewPage()),
    HomeData(title: 'ListView', subTitle: '上拉加载列表', routerPage: ListViewPullPage()),
    HomeData(title: 'ScrollController', subTitle: 'ScrollController滚动监听及控制', routerPage: ScrollControllerPage()),
    HomeData(
        title: 'ScrollNotification',
        subTitle: 'ListView滚动通知,NotificationListener<ScrollNotification>',
        routerPage: ScrollNotiPage()),
    HomeData(title: 'AnimatedList', subTitle: '列表中插入或删除节点时执行一个动画', routerPage: AnimatedListPage()),
    HomeData(title: 'GridView', subTitle: '创建横轴固定数量/固定最大长度', routerPage: GridViewPage()),
    HomeData(title: 'GridView.builder', subTitle: '动态创建子widget', routerPage: GridViewBuilderPage()),
    HomeData(
        title: 'PageView',
        subTitle: 'Tab换页效果、图片轮动、抖音上下滑切换视频，可滚动组件子项缓存 KeepAlive',
        routerPage: PageViewPage()),
    HomeData(
        title: 'TabBarView',
        subTitle: 'TabBar和TabBarView联动指定TabController',
        routerPage: TabBarViewPage()),
    HomeData(
        title: 'TabBarView',
        subTitle: 'TabBar和TabBarView联动，通常会创建一个 DefaultTabController作为它们共同的父级组件',
        routerPage: TabBarViewPage2()),
    HomeData(
        title: 'CustomScrollView',
        subTitle: '同个页面包含多个可滚动组件',
        routerPage: CustomScrollPage()),
    HomeData(title: 'CustomScrollView', subTitle: '', routerPage: CustomScrollViewRoute()),
    HomeData(title: 'SliverPersistentHeader', subTitle: '', routerPage: SliverPersistentHeaderPage()),
    HomeData(title: 'NestedScrollView', subTitle: '', routerPage: NestedScrollViewPage()),
    HomeData(title: 'SliverAppBar', subTitle: '', routerPage: SliverAppBarPage()),
    HomeData(title: 'NestedTabBarView', subTitle: '', routerPage: NestedTabBarViewPage()),
    HomeData(title: 'WillPopScope', subTitle: '导航返回拦截', routerPage: WillPopScopePage()),
    HomeData(title: 'InheritedWidget', subTitle: '数据共享', routerPage: InheritedWidgetPage()),
    HomeData(title: 'ThemePage', subTitle: '路由换肤', routerPage: ThemePage()),
    HomeData(
        title: 'ValuelistenableBuilder',
        subTitle: '监听数据源，数据源发生变化会重新执行builder',
        routerPage: ValuelistenableBuilderPage()),
    HomeData(title: 'FutureBuilder', subTitle: '异步请求数据', routerPage: FutureBuilderPage()),
    HomeData(title: 'StreamBuilder', subTitle: '异步请求数据', routerPage: StreamBuilderPage()),
    HomeData(title: 'Dialog', subTitle: '对话框', routerPage: DialogPage()),
    HomeData(title: 'Listener', subTitle: '监听原始触摸事件，手指按下、移动、抬起', routerPage: ListenerPage()),
    HomeData(
        title: 'GestureDetector',
        subTitle: '手势识别单机、双击、长按、拖动、缩放、GestureRecognizer',
        routerPage: GestureDetectorPage()),

  ];

  Widget _itemForRow(BuildContext context, int index) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => _datas[index].routerPage));
      },
      child: Column(
        children: [
          ListTile(
            title: Text(_datas[index].title),
            subtitle: Text(_datas[index].subTitle),
          ),
          Container(
            height: 0.5,
            color: Colors.grey,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Colors.blueAccent,
      ),
      // drawer: MyDrawer(), //抽屉
      body: ListView.builder(
        itemCount: _datas.length,
        itemBuilder: _itemForRow,
      ),
    );
  }
}


