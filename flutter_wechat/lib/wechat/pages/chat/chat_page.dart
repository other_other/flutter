import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/chat/chat.dart';
import 'package:flutter_wechat/wechat/pages/chat/search_bar.dart';
import 'package:flutter_wechat/wechat/pages/const.dart';
import 'package:http/http.dart' as http;

/** 混入
 *
 * */

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

//with AutomaticKeepAliveClientMixin<ChatPage>
class _ChatPageState extends State<ChatPage> with AutomaticKeepAliveClientMixin<ChatPage> {
  List<Chat> _datas = [];

  var _cancelConnect = false;

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive {
    return true;
  }

  @override
  void initState() {
    super.initState();

    //数据请求
    // getDatas().then((value) => print(value));

    getDatas().then((List<Chat> datas) {
      if(!_cancelConnect) {
        setState(() {
          _datas = datas;
        });
      }
    }).catchError((error) {
      print(error);
    }).whenComplete(() {
      print('完毕');
    }).timeout(Duration(seconds: 1)).catchError((timeout){
      _cancelConnect = true;
      print('超时:${timeout}');
    });
  }

  //async 异步 Future可选返回结果可能有值
  Future<List<Chat>> getDatas() async {
    _cancelConnect = false;

    var url = Uri.parse('http://rap2api.taobao.org/app/mock/data/2111273');
    //await 等待http.get返回
    final response = await http.get(url);

    if (response.statusCode == 200) {
      //转换成map类型
      final responseBody = json.decode(response.body);
      //转换模型数组
      List<Chat> chatList = responseBody['chat_list'].map<Chat>((item) {
        return Chat.fromJson(item);
      }).toList();

      return chatList;
    } else {
      throw Exception('statusCode:${response.statusCode}');
    }
  }

  Widget _buildItemForRow(BuildContext context, int index) {
    if (index == 0) {
      return SearchCell(datas: _datas);
    }
    return ListTile(
      title: Text(_datas[index-1].name),
      subtitle: Container(
        height: 20,
        child: Text(_datas[index-1].message,
            overflow: TextOverflow.ellipsis),
      ),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(_datas[index-1].imageUrl),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: WeChatThemeColor,
        centerTitle: true,
        elevation: 0.0,//导航底部线条隐藏
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10),
            child: PopupMenuButton(
              offset: Offset(0, 60),
              child: Image(
                image: AssetImage('images/圆加.png'),
                width: 25,
              ),
              itemBuilder: _buildPopMenueItem,
            ),
          ),
        ],
      ),
      body: Center(
        child: Container(
          child: _datas.length == 0
              ? Center(child: Text('loading..'))
              : ListView.builder(
                  itemCount: _datas.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildItemForRow(context, index);
                  }),
        ),
      ),
    );
  }
}

Widget _buildListItem(BuildContext context, int index) {
  return Text('data');
}

List<PopupMenuItem<String>> _buildPopMenueItem(BuildContext context) {
  return [
    _buildPopupItem('images/发起群聊.png', '发起群聊'),
    _buildPopupItem('images/添加朋友.png', '添加朋友'),
    _buildPopupItem('images/扫一扫1.png', '扫一扫'),
    _buildPopupItem('images/收付款.png', '收付款'),
  ];
}

PopupMenuItem<String> _buildPopupItem(String imageName, String title) {
  return PopupMenuItem(
      child: Row(
    children: [
      Image(image: AssetImage(imageName), width: 25),
      Container(width: 20),
      Text(title, style: TextStyle(color: Colors.white)),
    ],
  ));
}

/*
* FutureBuilder(
          future: getDatas(), //getDatas返回future
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.data);
            print(snapshot.connectionState); //链接状态
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: Text('loading...'),
              );
            } else {
              return ListView(
                children: snapshot.data.map<Widget>((item) {
                  return ListTile(
                    title: Text(item.name),
                    subtitle: Container(
                      height: 20,
                      child: Text(item.message, overflow: TextOverflow.ellipsis),
                    ),
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(item.imageUrl),
                    ),
                  );
                }).toList(),
              );
            }
          },
        )
* */ //FutureBuilder
