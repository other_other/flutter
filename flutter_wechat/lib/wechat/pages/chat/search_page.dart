import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/chat/chat.dart';
import 'package:flutter_wechat/wechat/pages/chat/search_bar.dart';

class SearchPage extends StatefulWidget {

  final List<Chat> datas;

  const SearchPage({Key? key, required this.datas}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  List<Chat> _models = [];//内容显示模型数组
  String _searchStr = ''; //用于匹配高亮显示内容

  void _searchData(String text) {
    if (text.length == 0) {
      _models = [];
    } else {
      _models.clear();
      for (int i = 0; i < widget.datas.length; i++) {
        if (widget.datas[i].name.contains(text)) {
          _models.add(widget.datas[i]);
        }
      }
    }
    _searchStr = text;
    setState(() {});
  }

  //高亮显示名字
  Widget _title(String name) {
    TextStyle normalStyle    = TextStyle(fontSize: 16, color: Colors.black);
    TextStyle highlighStyle = TextStyle(fontSize: 16, color: Colors.green);

    List<TextSpan> spans = [];
    //找到哪些是高亮的哪些是黑色的。
    List<String> strs = name.split(_searchStr);
    for (int i = 0; i < strs.length; i++) {
      String str = strs[i];
      if (str == '' && i < strs.length - 1) {//遇到空串就是高亮
        spans.add(TextSpan(text: _searchStr, style: highlighStyle));
      } else {
        spans.add(TextSpan(text: str, style: normalStyle));
        if (i < strs.length - 1) {
          //只要不是最后一个
          spans.add(TextSpan(text: _searchStr, style: highlighStyle));
        }
      }
    }

    return RichText(
        text: TextSpan(children: spans),
    );
  }

  Widget _itemForRow(BuildContext context, int index) {
    return ListTile(
      title: _title(_models[index].name),
      subtitle: Container(
        height: 20,
        child: Text(_models[index].message,
            overflow: TextOverflow.ellipsis),
      ),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(_models[index].imageUrl),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SearchBar(onChanged: (text) {
            _searchData(text);
          }), //顶部搜索条
          Expanded(
            flex: 1, //占据剩下所有空间
            child: MediaQuery.removePadding(
              context: context,
              removeTop: true, //移除列表顶部间距
              child: NotificationListener( //滚动监听
                onNotification: (ScrollNotification note) {
                  FocusScope.of(context).requestFocus(FocusNode());//收起键盘
                  return true;
                },
                child: ListView.builder(
                  itemCount: _models.length,
                  itemBuilder: _itemForRow,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
