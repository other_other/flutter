import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/chat/search_page.dart';
import 'package:flutter_wechat/wechat/pages/const.dart';

import 'chat.dart';

//首页顶部搜索Cell
class SearchCell extends StatelessWidget {

  final List<Chat> datas;

  const SearchCell({Key? key, required this.datas}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => SearchPage(datas: datas)));
      },
      child: Container(
        height: 44,
        padding: EdgeInsets.all(5),
        color: WeChatThemeColor,
        child: Stack(
          children: [
            Container(
              height: 34,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage('images/放大镜b.png'),
                    width: 20,
                    color: Colors.grey,
                  ),
                  Text(
                    '  搜索',
                    style: TextStyle(fontSize: 15, color: Colors.grey),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//搜索页面顶部搜索条
class SearchBar extends StatefulWidget {

  //回调
  final ValueChanged<String> onChanged;

  const SearchBar({Key? key, required this.onChanged}) : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {

  final TextEditingController _controller = TextEditingController();
  bool _showClear = false;

  _onChange(String text) {
    //回调
    widget.onChanged(text);
    if (text.length > 0) {
      _showClear = true;
      setState(() {});
    } else {
      _showClear = false;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: WeChatThemeColor,
      child: Column(
        children: [
          SizedBox(
            height: 40,
          ),//上半部分
          Container(
            height: 44,
            child: Row(
              children: [
                Container(
                  width: ScreenWidth(context) - 40,
                  height: 35,
                  padding: EdgeInsets.only(left: 5, right: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Row(
                    children: [
                      Image(
                          image: AssetImage('images/放大镜b.png'),
                          width: 20,
                          color: Colors.grey,
                      ),
                      Expanded(
                          flex: 1,
                          child: TextField(
                            controller: _controller,
                            onChanged: _onChange,
                            cursorColor: Colors.green, //输入光标颜色
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(left: 5, bottom: 10),
                              hintText: '搜索',
                            ),
                          ),
                      ),
                      _showClear ? GestureDetector(
                        onTap: (){
                          _controller.clear();
                          _onChange('');
                        },
                        child: Icon(
                            Icons.cancel,
                            size: 20,
                            color: Colors.grey,
                        ),
                      ) : Container(),//清除按钮
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Text('取消'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
