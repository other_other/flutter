import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/discover/discover_cell.dart';

class MinePage extends StatefulWidget {
  const MinePage({Key? key}) : super(key: key);

  @override
  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Container(
              color: Color.fromRGBO(220, 220, 220, 1.0),
              child: MediaQuery.removePadding(
                  removeTop: true,  //MediaQuery.removePadding 包装下去除ListView顶部间距
                  context: context,
                  child: ListView(
                    children: [
                      headerWidget(), //头部
                      SizedBox(height: 10),
                      DiscoverCell(title: '支付', imageName: 'images/微信支付1.png'),
                      SizedBox(height: 10),
                      DiscoverCell(title: '收藏', imageName: 'images/微信收藏.png'),
                      Row(
                        children: [
                          Container(width: 50, height: 0.5, color: Colors.white),
                          Container(height: 0.5, color: Colors.grey),
                        ],
                      ),
                      DiscoverCell(title: '相册', imageName: 'images/微信相册.png'),
                      Row(
                        children: [
                          Container(width: 50, height: 0.5, color: Colors.white),
                          Container(height: 0.5, color: Colors.grey),
                        ],
                      ),
                      DiscoverCell(title: '卡包', imageName: 'images/微信卡包.png'),
                      Row(
                        children: [
                          Container(width: 50, height: 0.5, color: Colors.white),
                          Container(height: 0.5, color: Colors.grey),
                        ],
                      ),
                      DiscoverCell(title: '表情', imageName: 'images/微信表情.png'),
                      SizedBox(height: 10),
                      DiscoverCell(title: '设置', imageName: 'images/微信设置.png'),
                    ],
                  ),
              ),
            ), //列表
            Container(
              margin: EdgeInsets.only(top: 40, right: 10),
              height: 25,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end, //横向靠右边布局
                children: [
                  Image(image: AssetImage('images/相机.png'))
                ],
              ),
            ), //相机
          ],
        ),
      ),
    );
  }

  Widget headerWidget() {
    return Container(
      height: 200,
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(top: 100),
        padding: EdgeInsets.all(10),
        child: Container(
          margin: EdgeInsets.only(left: 20),
          child: Row(
            children: [
              Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration( //装饰器 添加圆角
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(image: AssetImage('images/Hank.png')),
                ),
              ), //左边带圆角头像
              Expanded(//填充布局
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        height: 35,
                        child: Text('Hank',style: TextStyle(fontSize: 25)),
                      ),//微信昵称
                      Container(
                        height: 35,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('微信号：123456', style: TextStyle(
                                fontSize: 17,
                                color: Colors.grey)), //微信号
                            Image(image: AssetImage('images/icon_right.png'), width: 15), //箭头
                          ],
                        )
                      ),//微信号
                    ],
                  ),
                ),
              ), //右边部分
            ],
          ),
        ),
      ),
    );
  }
}
