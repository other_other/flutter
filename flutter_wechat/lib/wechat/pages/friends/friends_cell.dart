import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/const.dart';

class FriendCell extends StatelessWidget {
  const FriendCell({
    Key? key,
    required this.name,
    this.imageUrl,
    this.groupTitle,
    this.imageAssets,
  }) : super(key: key);

  final String? imageUrl;
  final String name;
  final String? groupTitle;
  final String? imageAssets;  //本地图片

  @override
  Widget build(BuildContext context) {

    //头像
    ImageProvider? _headerImage() {
      if (imageUrl != null) {
        return NetworkImage(imageUrl!);
      }
      if (imageAssets != null) {
        return AssetImage(imageAssets!);
      }
      return null;
    }

    return Container(
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 10),
            height: groupTitle != null ? 30 : 0,
            color: Color.fromRGBO(1, 1, 1, 0.0),
            child: groupTitle != null ? Text(groupTitle!, style: TextStyle(color: Colors.grey)) : null,
          ),
          Container(
            color: Colors.white,
            child: Row(
              children: [
                Container(
                  width: 34,
                  height: 34,
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.0),
                    image: _headerImage() != null ? DecorationImage(image: _headerImage()!) : null,
                  ),
                ), //图片
                Container(
                  height: 54,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          name,
                          style: TextStyle(fontSize: 18),
                        ),
                        alignment: Alignment.centerLeft,
                        height: 53.5,
                        width: ScreenWidth(context) - 54,
                      ), // 姓名
                      Container(
                        height: 0.5,
                        color: WeChatThemeColor,
                        width: ScreenWidth(context) - 54,
                      ), //分割线
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
