import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/const.dart';
import 'package:flutter_wechat/wechat/pages/discover/discover_child_page.dart';
import 'package:flutter_wechat/wechat/pages/friends/friends_cell.dart';
import 'package:flutter_wechat/wechat/pages/friends/friends_data.dart';
import 'package:flutter_wechat/wechat/pages/friends/index_bar.dart';

class FriendsPage extends StatefulWidget {
  const FriendsPage({Key? key}) : super(key: key);

  @override
  _FriendsPageState createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> with AutomaticKeepAliveClientMixin<FriendsPage> {

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive {
    return true;
  }

  //初始化数据
  @override
  void initState() {
    super.initState();
    //处理联系人列表数据
    _listDatas..addAll(datas)..addAll(datas);
    _listDatas.sort((Friends a,Friends b) {
      return a.indexLetter!.compareTo(b.indexLetter!);
    });
    _scrollController = ScrollController();

    var _groupOffSet = 54.0 * 4;
    for (int i = 0; i<_listDatas.length; i++) {
      if (i < 1) {
        //第一个一定是头部
        _groupOffsetMap.addAll({_listDatas[i].indexLetter: _groupOffSet});
        _groupOffSet += 84;
      } else if (_listDatas[i].indexLetter == _listDatas[i - 1].indexLetter) {
        //如果没有头
        _groupOffSet += 54;
      } else {
        //剩下的就是有头部的了
        _groupOffsetMap.addAll({_listDatas[i].indexLetter: _groupOffSet});
        _groupOffSet += 84;
      }
    }
  }

  //列表数据
  final List<Friends> _listDatas = [];
  late ScrollController _scrollController;
  final Map _groupOffsetMap = {
    INDEX_WORDS[0] : 0.0,
    INDEX_WORDS[1] : 0.0,
  };



  //顶部分组数据
  final List<Friends> _headerData = [
    Friends(imageUrl: 'images/新的朋友.png', name: '新的朋友'),
    Friends(imageUrl: 'images/群聊.png', name: '群聊'),
    Friends(imageUrl: 'images/标签.png', name: '标签'),
    Friends(imageUrl: 'images/公众号.png', name: '公众号'),
  ];

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: WeChatThemeColor,
        title: Text('通讯录', style: TextStyle(color: Colors.black)),
        elevation: 0.0, //隐藏底部阴影线条
        actions: [
          GestureDetector( //添加手势 用GestureDetector 包装下
            child: Container(
              margin: EdgeInsets.only(right: 10),
              child: Image(
                image: AssetImage('images/icon_friends_add.png'),
                width: 25,
              ),
            ),
            onTap: (){
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => DiscoverChildPage(title: '添加朋友')),
              );
            },
          ),
        ],
      ),
      body: Stack(
        children: [
          Container(
            color: WeChatThemeColor,
            child: ListView.builder(
              controller: _scrollController,//有个controller属性
              itemCount: _listDatas.length + _headerData.length,
              itemBuilder: _itemForRow,
            ),
          ), //通讯录列表
          IndexBar(
              indexBarCallback: (String str) {
                if (_groupOffsetMap[str] != null) {
                  _scrollController.animateTo(
                      _groupOffsetMap[str],
                      duration: Duration(milliseconds: 10),
                      curve: Curves.easeIn);
                }
              },
          ), //索引条
        ],
      ),
    );
  }

  Widget _itemForRow(BuildContext context, int index) {

    //头部4个Cell
    if (index < _headerData.length) {
      return FriendCell(
          name: _headerData[index].name,
          imageAssets: _headerData[index].imageUrl,
      );
    }

    final friendIndex = index - _headerData.length;

    //如果当前和上一个Cell的IndexLetter一样，就不显示头
    if(index > _headerData.length && _listDatas[friendIndex].indexLetter == _listDatas[friendIndex-1].indexLetter){
      return FriendCell(
        imageUrl: _listDatas[friendIndex].imageUrl,
        name: _listDatas[friendIndex].name,
      );
    }

    return FriendCell(
      imageUrl: _listDatas[friendIndex].imageUrl,
      name: _listDatas[friendIndex].name,
      groupTitle: _listDatas[friendIndex].indexLetter,
    );
  }

}

