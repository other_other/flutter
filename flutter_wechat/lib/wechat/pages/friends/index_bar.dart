import 'package:flutter/material.dart';

import '../const.dart';

class IndexBar extends StatefulWidget {
  //回调
  final void Function(String str) indexBarCallback;

  const IndexBar({Key? key, required this.indexBarCallback}) : super(key: key);

  @override
  _IndexBarState createState() => _IndexBarState();
}

//获取险种的坐标
int getIndex(BuildContext context, Offset globalPosition) {
  RenderBox box = context.findRenderObject() as RenderBox;
  //坐标转换，算出Y值！
  double y = box.globalToLocal(globalPosition).dy;
  //每一个Item的高度
  var itemHeight = ScreenHeight(context) / 2 / INDEX_WORDS.length;
  //clamp 限制范围最小最大
  int index = (y ~/ itemHeight).clamp(0, INDEX_WORDS.length - 1);
  print('当前选中的是：${INDEX_WORDS[index]}');
  return index;
}

class _IndexBarState extends State<IndexBar> {
  var _selectedIndex = -1;
  Color _bkColor = const Color.fromRGBO(1, 1, 1, 0.0); //背景色
  Color _textColor = Colors.black; //文字颜色

  String _indicatorText = 'A';//气泡显示文字
  //aligment的y值范围 -1.1-1.1 总共2.2
  double _indicatorY = 0.0;
  bool _indicatorHidden = true;

  @override
  Widget build(BuildContext context) {
    List<Widget> words = [];
    for (int i = 0; i < INDEX_WORDS.length; i++) {
      words.add(
        Expanded(
          //放Expanded平分大小布局
          child: Text(INDEX_WORDS[i],
              style: TextStyle(fontSize: 10, color: _textColor)),
        ),
      );
    }

    return Positioned(
      right: 0.0,
      height: ScreenHeight(context) / 2,
      top: ScreenHeight(context) / 8,
      width: 120,
      child: Row(
        children: [
          Container(
            alignment: Alignment(0, _indicatorY),
            width: 100,
            child: _indicatorHidden == true ? null : Stack(
              alignment: Alignment(-0.2, 0), //子部件相对位置
              children: [
                Image(image: AssetImage('images/气泡.png'), width: 60),
                Text(_indicatorText, style: TextStyle(
                  fontSize: 35,
                  color: Colors.white,
                )),
              ],
            ),
          ),
          GestureDetector(
            //添加手势
            child: Container(
              color: _bkColor, //背景颜色
              child: Column(
                children: words,
              ),
            ),
            //点击下去
            onVerticalDragDown: (DragDownDetails details) {
              int index = getIndex(context, details.globalPosition);
              if (index != _selectedIndex) {
                //去重
                _selectedIndex = index;
                widget.indexBarCallback(INDEX_WORDS[index]);
              }

              //内部气泡文字显示
              _indicatorText = INDEX_WORDS[index];
              _indicatorY = 2.2 / (INDEX_WORDS.length-1) * index - 1.1;
              _indicatorHidden = false;

              setState(() {
                _bkColor = const Color.fromRGBO(1, 1, 1, 0.5);
                _textColor = Colors.white;
              });
            },
            //更新
            onVerticalDragUpdate: (DragUpdateDetails details) {
              int index = getIndex(context, details.globalPosition);
              if (index != _selectedIndex) {
                //去重
                _selectedIndex = index;
                widget.indexBarCallback(INDEX_WORDS[index]);
              }

              //内部气泡文字显示
              _indicatorText = INDEX_WORDS[index];
              _indicatorY = 2.2 / (INDEX_WORDS.length-1) * index - 1.1;
              _indicatorHidden = false;
              setState(() {});
            },
            //点击完毕
            onVerticalDragEnd: (DragEndDetails details) {
              _bkColor = Color.fromRGBO(1, 1, 1, 0.0);
              _textColor = Colors.black;
              _selectedIndex = -1;
              _indicatorHidden = true;
              setState(() {});
            },
          )
        ],
      ),
    );
  }
}

const INDEX_WORDS = [
  '🔍',
  '☆',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z'
];
