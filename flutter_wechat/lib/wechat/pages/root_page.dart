import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/chat/chat_page.dart';

import 'discover/discover_page.dart';
import 'friends/friends_page.dart';
import 'mine_page.dart';


class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {

  final PageController _controller = PageController(
    initialPage: 0,
  );

  //底部tabbar
  List <BottomNavigationBarItem> items = [
    const BottomNavigationBarItem(
      label: '微信',
      //icon: Icon(Icons.chat), //Material 图标
      icon: Image(
          image: AssetImage('images/tabbar_chat.png'), width: 20, height: 20),
      activeIcon: Image(image: AssetImage('images/tabbar_chat_hl.png'),
          width: 20,
          height: 20),
    ),
    const BottomNavigationBarItem(
      label: '通讯录',
      icon: Image(image: AssetImage('images/tabbar_friends.png'),
          width: 20,
          height: 20),
      activeIcon: Image(image: AssetImage('images/tabbar_friends_hl.png'),
          width: 20,
          height: 20),
    ),
    const BottomNavigationBarItem(
      label: "发现",
      icon: Image(image: AssetImage('images/tabbar_discover.png'),
          width: 20,
          height: 20),
      activeIcon: Image(image: AssetImage('images/tabbar_discover_hl.png'),
          width: 20,
          height: 20),
    ),
    const BottomNavigationBarItem(
      label: '我',
      icon: Image(
          image: AssetImage('images/tabbar_mine.png'), width: 20, height: 20),
      activeIcon: Image(image: AssetImage('images/tabbar_mine_hl.png'),
          width: 20,
          height: 20),
    ),
  ];

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          onTap: (int index) {
            _currentIndex = index;
            setState(() {});
            _controller.jumpToPage(index);
          },
          selectedFontSize: 12.0,
          //选中字体大小
          type: BottomNavigationBarType.fixed,
          items: items,
          fixedColor: Colors.green,
          //选中index图标颜色
          currentIndex: _currentIndex,
        ),
        body: PageView(
          // onPageChanged: (int index) {
          //   _currentIndex = index;
          //   setState(() {});
          // },//拖拽滚动
          physics: NeverScrollableScrollPhysics(),//禁用拖拽滚动
          controller: _controller,
          children: [ChatPage(), FriendsPage(), DiscoverPage(), MinePage()],
        ),
      ),
    );
  }
}


