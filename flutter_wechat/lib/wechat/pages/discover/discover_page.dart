import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/discover/discover_cell.dart';

class DiscoverPage extends StatefulWidget {
  const DiscoverPage({Key? key}) : super(key: key);

  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {

  Color _themeColor = Color.fromRGBO(220, 220, 220, 1.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _themeColor,
        centerTitle: true,
        title: Text('发现', style: TextStyle(color: Colors.black)),
        elevation: 0.0, //去除appBar底部阴影线条
      ),
      body: Container(
        color: _themeColor,
        height: 800,
        child: ListView(
          children: [
            DiscoverCell(
                title: '朋友圈',
                imageName: 'images/朋友圈.png',
            ), //朋友圈
            SizedBox(height: 10), //分割
            DiscoverCell(
                title: '扫一扫',
                imageName: 'images/扫一扫2.png',
            ), //扫一扫
            Row(
              children: [
                Container(width: 50, height: 0.5, color: Colors.white),
                Container(height: 0.5, color: Colors.grey),
              ],
            ),
            DiscoverCell(
              title: '摇一摇',
              imageName: 'images/摇一摇.png',
            ), //摇一摇
            SizedBox(height: 10), //分割
            DiscoverCell(
                title: '看一看',
                imageName: 'images/看一看icon.png',
            ), //看一看
            Row(
              children: [
                Container(width: 50, height: 0.5, color: Colors.white),
                Container(height: 0.5, color: Colors.grey),
              ],
            ),
            DiscoverCell(
                title: '搜一搜',
                imageName: 'images/搜一搜 2.png',
            ), //搜一搜
            SizedBox(height: 10), //分割
            DiscoverCell(
                title: '附近的人',
                imageName: 'images/附近的人icon.png',
            ), //附近的人
            SizedBox(height: 10), //分割
            DiscoverCell(
              title: '购物',
              imageName: 'images/购物.png',
              subTitle: '618限时特价',
              subImageName: 'images/badge.png',
            ), //购物
            Row(
              children: [
                Container(width: 50, height: 0.5, color: Colors.white),
                Container(height: 0.5, color: Colors.grey),
              ],
            ),
            DiscoverCell(
                title: '游戏',
                imageName: 'images/游戏.png',
            ), //游戏
            SizedBox(height: 10), //分割
            DiscoverCell(
                title: '小程序',
                imageName: 'images/小程序.png'
            ), //小程序
          ],
        ),
      ),
    );
  }
}
