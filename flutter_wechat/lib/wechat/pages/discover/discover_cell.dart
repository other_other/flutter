import 'package:flutter/material.dart';
import 'package:flutter_wechat/wechat/pages/discover/discover_child_page.dart';

class DiscoverCell extends StatefulWidget {
  const DiscoverCell({
    Key? key,
    required this.title, //必选
    this.subTitle,
    required this.imageName,
    this.subImageName,
  }) :  assert(title != null, 'title 不能为空'),
        assert(imageName != null, 'imageName 不能为空'),
        super(key: key);

  final String title;
  final String? subTitle;  //非必要参数
  final String imageName;
  final String? subImageName;

  @override
  _DiscoverCellState createState() => _DiscoverCellState();
}

class _DiscoverCellState extends State<DiscoverCell> {

  Color _currentColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(  //添加手势装饰

      //Cell点击
      onTap: (){
        //带参数的路由跳转
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => DiscoverChildPage(title: widget.title),
            ),
        );
        setState(() => _currentColor = Colors.white);
      },

      onTapDown: (TapDownDetails details) => setState(() => _currentColor = Colors.grey),

      onTapCancel: () => setState(() => _currentColor = Colors.white),

      child: Container(
        padding: EdgeInsets.all(10), //内边距
        color: _currentColor,
        height: 54,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween, //主轴布局
          children: [
            Container(
              child: Row(
                children: [
                  Image(
                    image: AssetImage(widget.imageName),
                    width: 20,
                  ), // 图标
                  SizedBox( width: 15), // SizeBox 来设置两控件间距
                  Text(widget.title),   // 标题
                ],
              ),
            ), //left
            Container(
              child: Row(
                children: [
                  Text(
                    widget.subTitle != null ? widget.subTitle! : '',
                    style: TextStyle(color: Colors.grey),
                  ),
                  widget.subImageName != null ?
                  Container(
                    child: Image(
                      image: AssetImage(widget.subImageName!),
                      width: 15,
                    ),
                    margin: EdgeInsets.only(left: 10, right: 10), //添加图标外边距
                  ) : Container(),
                  Image(
                    image: AssetImage('images/icon_right.png'),
                    width: 15,
                  )
                ],
              ),
            ), //right
          ],
        ),
      ),
    );
  }
}
