

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_wechat/wechat/pages/root_page.dart';


void main() {
  debugPaintSizeEnabled = false; //可以看调试布局
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,//右上角debug图标隐藏
      theme: ThemeData(
        colorScheme: ColorScheme.light(
          primary: Colors.blue,
        ),
        primaryColor: Colors.blue, //这个属性不起作用了 要使用ThemeData的colorScheme属性
        highlightColor: const Color.fromRGBO(1, 0, 0, 0.0),//去掉高亮tabbar点击水波纹
        splashColor: const Color.fromRGBO(1, 0, 0, 0.0),//去掉tabbar点击水波纹
        cardColor: const Color.fromRGBO(1, 1, 1, 0.65),//popmenue的背景颜色
      ),
      home: const RootPage(),
    );
  }
}
