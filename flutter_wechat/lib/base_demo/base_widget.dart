
import 'package:flutter/material.dart';

class BaseDemo extends StatelessWidget {
  const BaseDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.yellow,
      child: Row(
        children: [
          Container(
            color: Colors.red,
            child: Icon(
              Icons.add,
              size: 45,
            ),
            //padding: EdgeInsets.only(left: 30, right: 30, top: 30, bottom: 30)
            padding: EdgeInsets.all(30),
            margin: EdgeInsets.all(10),
            height: 200,
            width: 200,
          )
        ],
      ),
    );
  }
}





