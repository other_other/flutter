


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'base_widget.dart';

/*
*
* */



void main() {
  runApp(App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //右上角 debug 角标显示
      home: Home(),
      theme: ThemeData(
        primaryColor: Colors.green, //主题颜色
      ),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green, //页面背景颜色
      appBar: AppBar(
        title: Text('Demo'),
        foregroundColor: Colors.red, //appBar上标题文字背景
        backgroundColor: Colors.blue, //AppBar背景
      ),
      body: BaseDemo(),
    );
  }
}



































