



```dart
return MaterialApp(
  title: 'Flutter Demo',
  theme: ThemeData(
    primaryColor: Colors.blue,//这个属性不起作用了 要使用ThemeData的colorScheme属性
    highlightColor: const Color.fromRGBO(1, 0, 0, 0.0),//去掉高亮tabbar点击水波纹
    splashColor: const Color.fromRGBO(1, 0, 0, 0.0),//去掉tabbar点击水波纹
  ),
  home: const RootPage(),
);
```

安卓App图标、启动图标文件配置

目录：android/app/src/main/res/

```dart
//mipmap-mdip 对应 1x，mipmap-hdip 对应1.5x，mipmap-xhdip 对应2x
//mipmap-xxhdip 对应3x mipmap-xxxhdip 对应4x

//drawable/launch_background.xml 配置启动图标名称
<item>
    <bitmap
        android:gravity="center"
        android:src="@mipmap/launch_image" />
</item>
//AndroidManifest.xml 配置APP图标、名称
android:label="微信"
android:icon="@mipmap/app_icon">       
```

图片 pubspec.yaml 配置

```dart
flutter:
  uses-material-design: true
  # 添加下面的
  assets:
    - images/
```

去除appBar底部阴影线条

```dart
appBar: AppBar(
  backgroundColor: _themeColor,
  centerTitle: true,
  title: Text('发现', style: TextStyle(color: Colors.black)),
  elevation: 0.0, //去除appBar底部阴影线条
),
```

添加手势操作

```dart
@override
Widget build(BuildContext context) {
  return GestureDetector(  //添加手势装饰
    //Cell点击
    onTap: (){
      //带参数的路由跳转
      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => DiscoverChildPage(title: widget.title),
          ),
      );
      setState(() => _currentColor = Colors.white);
    },
```

ListView 顶部有间距，将ListView包装下

```dart
child: MediaQuery.removePadding(
    removeTop: true,//MediaQuery.removePadding 包装下去除ListView顶部间距
    context: context,
    child: ListView(
```

图片带圆角

```dart
Container(
  width: 70,
  height: 70,
  decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(10.0),
    image: DecorationImage(image: AssetImage('images/Hank.png')),
  ),
), //带圆角头像
```

获取屏幕宽度

```dart
MediaQuery.of(context).size.width
```

导航右上角按钮

```dart
appBar: AppBar(
  backgroundColor: WeChatThemeColor,
  title: Text('通讯录', style: TextStyle(color: Colors.black)),
  elevation: 0.0, //隐藏底部阴影线条
  actions: [
    GestureDetector( //添加手势 用GestureDetector 包装下
      child: Container(
        margin: EdgeInsets.only(right: 10),
        child: Image(
          image: AssetImage('images/icon_friends_add.png'),
          width: 25,
        ),
      ),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => DiscoverChildPage(title: '添加朋友')));
      },
    ),
  ],
),
```

网络图像、本地图像

```dart
ImageProvider? _headerImage() {
  if (imageUrl != null) {
    return NetworkImage(imageUrl!);
  }
  if (imageAssets != null) {
    return AssetImage(imageAssets!);
  }
  return null;
}
```

RAP 接口管理平台

http://rap2.taobao.org/account/login

147822036@qq.com

ios123456

mockjs.com/examples.html



http 是官方请求的包

网络请求用dio包



Json Map 转换

```dart
import 'dart:convert';

final chat = {
	'name': '名字',
}
//map转json
final chatJson = json.encode(chat);
//json转map
final chatMap = json.decode(chatJson);

```

FutureBuilder 渲染异步数据

AutomaticKeepAliveClientMixin 保留状态































